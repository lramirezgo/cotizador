<div class="container"  ng-controller="appCtrllogin as cl">
<div class="login-box">
  <div class="login-logo">
    <a style="color:#FFFFFF"; href="../../index2.html">Fabrica de trofeos<b> Alianza</b></a>
    <p><img src="res/imagenes/logo.png" alt="Logotipo APR2" width="70px" height="70px"></p>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Ingresa usuario y contraseña para acceder al sistema</p>

   
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Numero empleado" ng-model="log.username" >
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Contraseña" ng-model="log.contrasena" >
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="button" class="btn btn-primary btn-block btn-flat"  ng-click="login()" >Entrar</button>
          <div class="box-body">

          
                
              
        </div>
        <!-- /.col -->
      </div>


      

    <div class="social-auth-links text-center">
     
    </div>
    <!-- /.social-auth-links -->

    <a href="#">Olvide mi contraseña</a><br>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</div>