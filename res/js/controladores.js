var app = angular.module("app", ['ngMaterial']).config(function($mdDateLocaleProvider) {
    
        // Example of a French localization.
        $mdDateLocaleProvider.months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
       var myShortMonths = $mdDateLocaleProvider.shortMonths = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
        $mdDateLocaleProvider.days = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
        $mdDateLocaleProvider.shortDays = ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi' ,'Sa'];
    
       
        // Can change week display to start on Monday.
        $mdDateLocaleProvider.firstDayOfWeek = 1;
    
        // Optional.
       // $mdDateLocaleProvider.dates = [1, 2, 3, 4, 5, 6, 7];
    
        // Example uses moment.js to parse and format dates.
        $mdDateLocaleProvider.parseDate = function(dateString) {
          var m = moment(dateString, 'L', true);
          return m.isValid() ? m.toDate() : new Date(NaN);
        };
    
        $mdDateLocaleProvider.formatDate = function(date) {
          var m = moment(date);
          return m.isValid() ? m.format('L') : '';
        };
    
        $mdDateLocaleProvider.monthHeaderFormatter = function(date) {
          return myShortMonths[date.getMonth()] + ' ' + date.getFullYear();
        };
    
        // In addition to date display, date components also need localized messages
        // for aria-labels for screen-reader users.
    
        $mdDateLocaleProvider.weekNumberFormatter = function(weekNumber) {
          return 'Semaine ' + weekNumber;
        };
    
        $mdDateLocaleProvider.msgCalendar = 'Calendrier';
        $mdDateLocaleProvider.msgOpenCalendar = 'Ouvrir le calendrier';
    
        
        });

app.controller("appCtrllay", ['$scope', '$http', '$location',function($scope, $http , $location){
   

    $scope.logout= {};
    $scope.datos_inicio= {};


    $scope.cerrar_sesion = function(){
        
             console.log("cerrar sesion");
             $scope.logout.operacion = "logout";
             $http.post("?action=access", $scope.logout)
             .then(function(res1){
                 console.log(res1);
                 console.warn(res1.data);
                 window.location="?view=login";
                
             });       
                
    };

    $scope.ir_pantalla = function(){
      console.log("entre a buscar pantalla");
      console.log( $scope.datos_inicio.buscar_pantalla);

      //window.location="?view=nota_remision"
    };

    }]);
    
    
app.controller("appCtrllogin", ['$scope', '$http', '$location',function($scope, $http , $location){
      
                

                $scope.log = {};
        
                $scope.login = function(){
               
                $scope.log.operacion = "login";
                    
                   $http.post("?action=access", $scope.log)
                    .then(function(res1){
                        console.warn(res1.data);
                        $scope.log.nombre = res1.data.nombre;
        
                        if(res1.data.error == true){
                            console.error("Error a logearse, usuario o contraseña no existe");
                            new PNotify({
                              title: 'Usuario o contraseña incorrecta',
                              text: '',
                              type: 'error',
                              hide: true,
                              styling: 'bootstrap3'});
                            
                        }else{
                           // window.location="?view=pantalla_fichas";
                          window.location="?view=home";
                            //$scope.$emit('cargar_menu', 1);
        
        
                        }
                        
                    
                        
                    }); 
                        
            };
    
        }]);
app.controller("appCtrlnota_remision", ['$scope', '$http', '$location',function($scope, $http , $location){

  $scope.combo_clientes = {};
  $scope.buscar_datos_clientes = {};
  $scope.busca_folio = {};
  $scope.combo_productos = {};
  $scope.agregar_linea_rem = {};
  
  $scope.llenar_combo_clientes = function(){
    console.log("entre a buscar combo");
     ///busco la informacion del usuario
    $scope.combo_clientes.operacion = 'bucar_clientes';
    $http.post("?action=clientes", $scope.combo_clientes)
            .then(function(res_combo_clientes){
               $scope.combo_clientes.datoos = res_combo_clientes.data;
                console.log($scope.combo_clientes.datoos); 
               //$scope.user.importe= res_combo.data.descripcion;
               
            },function(combo_clientes){
               // console.warn("segundo");
                //console.error(res);
            });

  };

  $scope.seleccionar_cliente = function(posicon){
  console.log("entre a seleccionar cliente");
  console.log( $scope.combo_clientes.cliente.codigo_cliente);

 $scope.buscar_datos_clientes.codigo_cliente = $scope.combo_clientes.cliente.codigo_cliente;
  $scope.buscar_datos_clientes.operacion = 'bucar_datos_cliente';
  $http.post("?action=clientes", $scope.buscar_datos_clientes)
          .then(function(res_buscar_datos_clientes){
             $scope.buscar_datos_clientes.datoos = res_buscar_datos_clientes.data;
              console.log($scope.buscar_datos_clientes.datoos[0].direccion); 
              $scope.buscar_datos_clientes.direccion = $scope.buscar_datos_clientes.datoos[0].direccion;
              $scope.buscar_datos_clientes.telefono = $scope.buscar_datos_clientes.datoos[0].telefono;
             //$scope.user.importe= res_combo.data.descripcion;
             
          },function(res_buscar_datos_clientes){
             // console.warn("segundo");
              //console.error(res);
          });


  };

  $scope.llenar_folio_remision = function(){
  console.log("entre a buscar folio");
   ///busco la informacion del usuario
  $scope.busca_folio.operacion = 'busca_folio_siguiente';
  $http.post("?action=cabecera_remision", $scope.busca_folio)
          .then(function(res_busca_folio){
             $scope.busca_folio.datoos = res_busca_folio.data;
              $scope.busca_folio.folio = $scope.busca_folio.datoos.folio;
             //$scope.user.importe= res_combo.data.descripcion;
             
          },function(res_busca_folio){
             // console.warn("segundo");
              //console.error(res);
          });

  };

  $scope.llenar_combo_produtos = function(){
  console.log("entre a buscar combo productos");
   ///busco la informacion del usuario
  $scope.combo_productos.operacion = 'bucar_productos';
  $http.post("?action=productos_pt", $scope.combo_productos)
          .then(function(res_combo_productos){
             $scope.combo_productos.datoos = res_combo_productos.data;
              console.log($scope.combo_productos.datoos); 
             //$scope.user.importe= res_combo.data.descripcion;
             
          },function(res_combo_productos){
             // console.warn("segundo");
              //console.error(res);
          });

  };
  $scope.agregar_producto = function(){
  console.log("entre a agregar producto");
  console.log($scope.combo_productos.producto_sel);

  $scope.agregar_linea_rem.folio_rem_pt =  $scope.busca_folio.folio;
  $scope.agregar_linea_rem.cantidad;
  $scope.agregar_linea_rem.codigo_pt = $scope.combo_productos.producto_sel.codigo_pt;
  $scope.agregar_linea_rem.precio_unitario = $scope.combo_productos.producto_sel.precio_unitario;

  console.log($scope.agregar_linea_rem.folio_rem_pt);
  console.log($scope.agregar_linea_rem.cantidad);
  console.log($scope.agregar_linea_rem.codigo_pt);
  console.log($scope.agregar_linea_rem.precio_unitario);

  $scope.agregar_linea_rem.operacion = 'guardar_productos';
  $http.post("?action=agregar_linea_rem", $scope.agregar_linea_rem)
          .then(function(res_agregar_linea_rem){
             $scope.agregar_linea_rem.datoos = res_agregar_linea_rem.data;
              console.log($scope.agregar_linea_rem.datoos); 
             //$scope.user.importe= res_combo.data.descripcion;
             
          },function(res_agregar_linea_rem){
             // console.warn("segundo");
              //console.error(res);
          });

  }


  $scope.llenar_combo_clientes();
  $scope.llenar_folio_remision();
  $scope.llenar_combo_produtos();

        
  }]).config(function($mdThemingProvider) {
      $mdThemingProvider.theme('dark-grey').backgroundPalette('grey').dark();
      $mdThemingProvider.theme('dark-orange').backgroundPalette('orange').dark();
      $mdThemingProvider.theme('dark-purple').backgroundPalette('deep-purple').dark();
      $mdThemingProvider.theme('dark-blue').backgroundPalette('blue').dark();
        });
    
    
    
    